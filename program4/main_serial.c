#include "image_template.h"
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "math.h"
#include <unistd.h> 

// Swaps values of two input floats
void swap(float *a, float *b)
{
	float t= *a;
	*a = *b;
	*b = t;
}

int comp (const void * elem1, const void * elem2) 
{
    float f = *((float*)elem1);
    float s = *((float*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

// outBound_check(): Returns 1 if the current position in kernal is good. Returns 0 if out of bounds;
// Use in main() by mupltiplying function with SOP iteration
// arguments (i,j,k,m, img_h, img_w, row_offset_i, col_offset_j) //ker_h, ker_w, ) ??
int outBound_check(int i, int j, int img_h, int img_w, int o_i, int o_j) 
{
	// check left boundry
	if((j+ o_j)<0)
		return 0;
	//check right
	else if((j+o_j)>=img_w)
		return 0;
	//check top
	else if((i+o_i)<0)
		return 0;
	//check bottom
	else if((i+o_i)>=img_h)
		return 0;
	else
		return 1; // should return 1 if all checks pass
}

void gaussian_kernal(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedcode to create kernals
    // [G,w]=GaussianKernel(sigma) // return values
    a= round(2.5*sigma - 0.5); //a=1
    *w=2*a+1; sum=0;  // w=3 
    *G = (float *)malloc(sizeof(float)*(*w));
    for (i=0; i<*w; i++){
        (*G)[i]=exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum+(*G)[i];      
    }
    
    //printf("Gausian_kernal: ");
    for (i=0; i<*w; i++){
        (*G)[i] = (*G)[i]/sum; //Normalize to get PDF area of 1
        //printf("%f	",(*G)[i]);// Print Gaussian mask 
    }
    //printf("\n");
}

void gaussian_derivative(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedocode to create derivative kernals
    // [Gderiv,w]=GaussianDerivativeKernel(sigma) // returns values
    a=round(2.5*sigma - 0.5);
    *w=2*a+1; sum=0;
    *G = (float *)malloc(sizeof(float)*(*w)); // this means passed in value is not yet malloc'ed
    for(i=0; i<*w; i++){   
        (*G)[i]=-2*(i-a)*exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum-i*(*G)[i];
    }  
    
    //printf("Gausian_derivative_kernal: ");
    for (i=0; i<*w; i++) {
        (*G)[i] = (*G)[i]/sum; //     Gderiv=Gderiv/sum 
        //printf("%f	",(*G)[i]);  // print Gaussian derivative
    }
    //printf("\n");
}

void convolve(float *img, float *ker, int width, int height, int k_w, int k_h, float *out_img)// pass in pointer of Image, pointer of G_gauss and/or G_deriv, image_width/length, kernal_width/height, double pointer to output image 
{

	float sum;
    int offset_i, offset_j;
    int i,j,k,m;
	
    //Begin looping thru image (chunk)
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            sum =0. ; // reset SOP at each image pixel
            // begin looping thru kernal element
            for(k=0;k<k_h;k++)
            { 
                for(m=0;m<k_w;m++)
                {
                    offset_i = -1*floor(k_h/2)+k;
                    offset_j = -1*floor(k_w/2)+m;
                    int tempCheck = outBound_check(i,j,height,width,offset_i,offset_j);
                    if( tempCheck==1)
                    {
                        sum += img[((i+offset_i)*width)+(j+offset_j)]*ker[k*k_w+m];
                        //if(k>2) printf("k:%d m:%d\n Value of Image:%f Kernal:%f \n", k, m,img[i*width+j],ker[k*k_w+m]);
                    }
                    else
                        sum += 0;
                    
                }
            }
            out_img[i*width+j] = sum;
        }
    }
}

void mag_phase(int height, int width, float *h, float *v, float *magnitude, float *phase) //argument should contain img_height, img_width, grad_horz, grad_vert, *magnitude
{
	for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
			magnitude[i*width + j]=sqrt( pow(v[i*width +j],2) + pow(h[i*width +j],2));
            phase[i*width + j]=atan2(v[i*width + j], h[i*width + j]);
		}
	}
}

void suppression(int height, int width,float *G_m, float *G_p, float *suppress)
{
    //suppress = *G_m;
    memcpy(suppress, G_m, width * height * sizeof(float));
    float theta;
    int check1, check2;
    int row_offset_i, col_offset_j;
    
    for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
            check1 = 0, check2 =0; // reset comparison check values to zero
            theta  = G_p[i*width +j];
            if (theta <0)
                theta += M_PI;
        
            theta = (180/M_PI)*theta;
            
			// will need to check for boundry within each IF statement
            if (theta <= 22.5||theta>157.5)
            {
                //  compare G_m(i,j) with left
                row_offset_i = 0; col_offset_j = -1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j)) // if in image
                    check1 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                //  compare G_m(i,j) with right
                row_offset_i = 0; col_offset_j = 1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check2 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                if(check1||check2) 
                    suppress[i*width +j]=0; // suppress[i*width +j]=0 if less than left or right
            }
            else if(theta>22.5 && theta<=67.5)
            {
                //compare G_m with(i,j) top_left
                row_offset_i = -1; col_offset_j = -1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check1 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                //  compare G_m(i,j) with bot_right
                row_offset_i = 1; col_offset_j = 1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check2 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                if(check1||check2) 
                    suppress[i*width +j]=0; // suppress(x,y)=0 if less than top_left or bot_right
            }
            else if(theta>67.5 && theta<=112.5)
            {
                //  compare G_mi,j) with top
                row_offset_i = -1; col_offset_j = 0;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check1 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                //  compare G_m(i,j) with bot
                row_offset_i = 1; col_offset_j = 0;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check2 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                if(check1||check2) 
                    suppress[i*width +j]=0; // suppress[i*width +j]=0 if less than top or bot
            }
            else if(theta>112.5 && theta<=157.5)
            {
                //  compare G_m(i,j) with top_right
                row_offset_i = -1; col_offset_j = 1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check1 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                //  compare G_m(i,j) with bot_left
                row_offset_i = 1; col_offset_j = -1;
                if(outBound_check(i,j,height,width, row_offset_i, col_offset_j))
                    check2 = G_m[i*width + j] < G_m[( (i+row_offset_i)*width )+(j+col_offset_j)];
                if(check1||check2) 
                    suppress[i*width +j]=0; // suppress[i*width +j]=0 if less than top_right or bot_left
            }
        }
    }
}

void edge(int height, int width,float *suppress, float *hyst, float *edges)
{
	int pct90, t_low, t_high; // used to define threashold foir hysteresis
    int tl, t, tr, l, r, bl, b, br, checkSum; // bool values to check if attached to 255
    int row_offset_i, col_offset_j; // offsets used to check if within image bounds
    float *sortedSuppress  =  (float *)malloc(sizeof(float)*width*height);
    //float sortedSuppress[10] = {4,5,2,3,1,0,9,8,6,7};// =  (int *)malloc(sizeof(int)*10);
    //sortedSuppress = {4,5,2,3,1,0,9,8,6,7};
    memcpy(sortedSuppress, suppress, width * height * sizeof(float) );
	/*
		Sort suppress pixels 
		http://stackoverflow.com/questions/1787996/c-library-function-to-do-sort
            qsort (sortedSuppress, si zeof(x)/sizeof(*x), sizeof(*x), comp);
		t_high = 90th percentile of sorted values
		t_low = t_high/5	
	*/
    qsort (sortedSuppress, width*height, sizeof(float), comp);
    //for (int i = 0 ; i < 10 ; i++)
        //printf ("%f ", sortedSuppress[i]);
	pct90   = 0.9*width*height;
    t_high  = sortedSuppress[ pct90 ];
    t_low   = t_high/5;
    // hyst = *suppress;
	//     memcpy(hyst, suppress, width * height * sizeof(float));
    
	// LOOP thru suppress pixels
    for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
			if(suppress[i*width +j] >= t_high)
			{
				hyst[i*width +j] = 255;
			}	
			else if(suppress[i*width +j] <= t_low)
			{
				hyst[i*width +j] = 0;
			}
			else
			{
				hyst[i*width +j] = 125;
			}
		}	
	}
	// LOOP thru hyst pixels (after suppress loop complete)
    for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
			if(hyst[i*width +j] == 125)
			{
                tl = t = tr = l = r = bl = b = br = 0; // reset neighbor check for pixel
                row_offset_i = -1; col_offset_j = -1; // check top-left
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    tl = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = -1; col_offset_j = 0; // check top
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    t  = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = -1; col_offset_j = 1; // check top-right
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    tr = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = 0; col_offset_j = -1; // check left
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    l  = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = 0; col_offset_j = 1; // check right
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    r  = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = 1; col_offset_j = -1; // check bottom-left
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    bl = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    
                row_offset_i = 1; col_offset_j = 0; // check bottom
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    b  = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                    char hystfName[100];
                row_offset_i = 1; col_offset_j = 1; // check bottom-right
                if ( outBound_check(i,j,height,width, row_offset_i, col_offset_j) )
                    br = hyst[( (i+row_offset_i)*width )+(j+col_offset_j)]==255;
                
                checkSum = tl + t + tr + l + r + bl + b + br;
				// NOTE: Must check for boudry conditions in 3x3 neighbors
				// IF (any of 3x3 neighbors == 255) 
				//		edges[i*width +j] = 255;
				// ELSE
				//		edges[i*width +j] = 0;
                if (checkSum >=1)
                    edges[i*width +j] = 255;
                else
                    edges[i*width +j] = 0;
			}
			else
                edges[i*width +j] = hyst[i*width +j];
		}	
	}
	free(sortedSuppress);
}
int main(int argc, char **argv){
    char fName[100] = "../LENNA_IMAGES/Lenna_org_256.pgm"; //default input image if none comand line argument
    // output filenames
    char magfName[100];// = "horizontal_gradient.pgm", 
    char phasefName[100];// = "vertical_gradient.pgm"; 
    char suppfName[100]; //
    char hystfName[100];
    char edgesfName[100];
    char picNum[4];
	float *temp_vert, *temp_horz, *h_Gradient, *v_Gradient, *image;
    float *G_kernal, *G_deriv, sigma =0.5; // STEP 1  
	float *magnitude, *phase;	//STEP 2
	float *suppress;	//STEP 3
	float *hyst, *edges; //STEP 4
    int im_width, im_height,w,w_ker, h_ker,w_der, h_der, num_threads = 4;
    int t,rc, remainder;
	struct timeval start, end; // for timing
    
	if (argc !=3)
    {
        printf("Correct argument list for main_serial.c is: ./exec <imagePath> <sigma value>\n");
	 	return 0;
    }
	if (argc >1){
            strcpy(fName, argv[1]);
            sigma = atof(argv[2]);
    }
    //Start timing
    gettimeofday(&start, NULL);
    // Read image using image_template.h function
    read_image_template(fName, &image, &im_width, &im_height);
    
    //Define output filenames based on test parameters
    strcpy(magfName, "./test_img/magnitude_");
    sprintf(picNum,"%d_s%.2f",im_width,sigma); strcat(magfName, picNum);
    strcat(magfName, ".pgm");
    
    strcpy(phasefName, "./test_img/phase_");
    strcat(phasefName, picNum);
    strcat(phasefName, ".pgm");
    
	strcpy(suppfName, "./test_img/suppress_");
    strcat(suppfName, picNum);
    strcat(suppfName, ".pgm");
	
    strcpy(hystfName, "./test_img/hyst_");
    strcat(hystfName, picNum);
    strcat(hystfName, ".pgm");
    
    strcpy(edgesfName, "./test_img/edges_");
    strcat(edgesfName, picNum);
    strcat(edgesfName, ".pgm");
	// VERTICAL GRADIENT
	printf("\n**** VERTICAL GRADIENT ****\n");
    // Create horizontal gaussian kernal from sigma
    gaussian_kernal(sigma, &G_kernal, &w); 
	w_ker = w; h_ker = 1; // define kernal dimensions for specific gradient
	printf("kernal width:%d height:%d\n",w_ker,h_ker);
    
	// create vertical derivative kernal
    gaussian_derivative(sigma, &G_deriv, &w);
	w_der = 1; h_der = w; 
	printf("derivative width:%d height:%d\n",w_der,h_der);
    for (int i=0; i<w/2; i++){ // Flip derivative kernal
		swap(&G_deriv[i], &G_deriv[w-i-1]);
	}
    /*printf("Gausian_derivative_kernal flipped: ");
    for (int i=0; i<w; i++)
        printf("%f	",G_deriv[i]);  // print flipped Gaussian derivative
    printf("\n");*/
	
	char tempName[100] = "step1_vert.pgm";
    temp_vert = (float *)malloc(sizeof(float)*im_width*im_height);
    convolve(image, G_kernal, im_width, im_height, w_ker, h_ker, temp_vert); 
	//write_image_template<float>(tempName, temp_vert, im_width, im_height);
    v_Gradient = (float *)malloc(sizeof(float)*im_width*im_height);
	convolve(temp_vert, G_deriv, im_width, im_height, w_der, h_der, v_Gradient);
    //write_image_template<float>(vertfName, v_Gradient, im_width, im_height);
	free(G_kernal); // free mask memory as they will be malloc'ed again when kernal; functions are called again
	free(G_deriv);
	
	// HORZONTAL GRADIENT
	printf("\n**** HORIZONTAL GRADIENT ****\n");
    // Create vertical gaussian kernal from sigma
    gaussian_kernal(sigma, &G_kernal, &w); 
	w_ker = 1; h_ker = w; // define kernal dimensions for specific gradient
	printf("kernal width:%d height:%d\n",w_ker,h_ker);
    
	// create horizontal derivative kernal
    gaussian_derivative(sigma, &G_deriv, &w);
	w_der = w; h_der = 1; 
	printf("derivative width:%d height:%d\n",w_der,h_der);
    for (int i=0; i<w/2; i++){ // Flip derivative kernal
		swap(&G_deriv[i], &G_deriv[w-i-1]);
	}
    /*printf("Gausian_derivative_kernal flipped: ");
    for (int i=0; i<w; i++)
        printf("%f	",G_deriv[i]);  // print flipped Gaussian derivative
    printf("\n");*/
	
	strcpy(tempName, "step1_horz.pgm");
    temp_horz = (float *)malloc(sizeof(float)*im_width*im_height);
    convolve(image, G_kernal, im_width, im_height, w_ker, h_ker, temp_horz);
	//write_image_template<float>(tempName, temp_horz, im_width, im_height);
    h_Gradient = (float *)malloc(sizeof(float)*im_width*im_height);
	convolve(temp_horz, G_deriv, im_width, im_height, w_der, h_der, h_Gradient);
    //write_image_template<float>(horzfName, h_Gradient, im_width, im_height);
	
	//Call magn_phase() function for image
	magnitude = (float *)malloc(sizeof(float)*im_width*im_height);
    phase = (float *)malloc(sizeof(float)*im_width*im_height);
	mag_phase(im_height, im_width, h_Gradient, v_Gradient, magnitude, phase);//argument should contain img_height, 	img_width, grad_horz, grad_vert, &magnitude, 
	///////write_image_template<float>(magfName, magnitude, im_width, im_height);
	///////write_image_template<float>(phasefName, phase, im_width, im_height);

    /**** Section to call suppression(magnitude, phase)*/
    suppress = (float *)malloc(sizeof(float)*im_width*im_height);
    suppression(im_height, im_width, magnitude, phase, suppress);
    ///////write_image_template<float>(suppfName, suppress, im_width, im_height);
    
    /**** Section to call edge()
     *      Double Threshold and Hysteresis*/
    hyst = (float *)malloc(sizeof(float)*im_width*im_height);
    edges = (float *)malloc(sizeof(float)*im_width*im_height);
    edge(im_height, im_width, suppress, hyst, edges);
    write_image_template<float>(hystfName, hyst, im_width, im_height);
    write_image_template<float>(edgesfName, edges, im_width, im_height);
    
	free(G_kernal);
	free(G_deriv);
	free(temp_horz);
	free(temp_vert);
	free(h_Gradient);
	free(v_Gradient);
	free(magnitude);
	free(phase);
    free(suppress);
    free(hyst);
    free(edges);
    // End Timing
    gettimeofday(&end, NULL);
    printf("\n\033[32;1m Time to run: %ld milliseconds\033[0m\n\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000);
	return 0;
}
