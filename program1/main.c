#include "image.h"
#include <time.h>
#include <sys/time.h>

void pad_chunks(int s_index, int height, int width, int *image , int chunk_count, int *chunk, char *pic_name){
    /***************************
     variables: 
     s_index - starting index of current chunk within *image
     height - height of current chunk
     width - width of chunk/image
     image - array to 1D rep of image
     chunk_count - total number of chunks
     *chunk - 1D array of current chunk 
     pic_name - name of chunk (for file output)
     NOTES:
        Function needs to
        1) take in the passed chunk
        2) determine where chunk is inside of full picture
        3) read rows from other chunks
        4) padd current chunk with other chunks
        5) Pass chunk back??? or write chunk to image file
        
        value 0 == black, value 255 == white
    */
        
        int *p_chunk;
        int size = (height+2)*(width+2);
        // index of picture 0,1,2,3 etc..
        int index = s_index/(height*width) ;
        int i,j,src_cntr = 0, tmp;
        p_chunk = (int *)malloc(sizeof(int)*size);
        
        p_chunk[0] = image[src_cntr+index*height*width+1];
        // Padding for top chunk
        for (i=1; i<(width+2);i++){
            // top padding
            p_chunk[i] = image[src_cntr+index*height*width];
            src_cntr++;
        }
        p_chunk[width+1] = image[src_cntr+index*height*width-2];
        src_cntr = 0;
        // middle padding
        for(i; i<size-(width+1); i++){
            p_chunk[i] = image[src_cntr+index*height*width];
            tmp = i+1;
            for(j = i+1;j< tmp+width;j++){
                p_chunk[j] = image[src_cntr+index*height*width];
                i++;
                src_cntr++;
            }
            p_chunk[++i] = image[src_cntr+index*height*width-1];
        }
        //p_chunk[size-width-2] = image[src_cntr+index*height*width+2];
        //p_chunk[size-width+1] = image[src_cntr+index*height*width];
        // Bottom padding
        i = size - (width+1)+1;
        src_cntr = src_cntr - ((width*2));
        //printf("%d %d\n", src_cntr, height*width); 
        for(i; i< size; i++){
            p_chunk[i] = image[src_cntr+index*height*width+1];
            src_cntr++;
        }
        p_chunk[size-1] = image[src_cntr+index*height*width-1];
    write_image_template(pic_name, p_chunk, width+2, height+2);
}

void split_chunks(int s_index, int height, int width, int *image, int chunk_count){
   // *image=(int *)malloc(sizeof(int)*(*im_width)*(*im_height));
    int *im_chunk;
    int i;
    char pic_num[4];
    char pic_name[100];
    // index of picture 0,1,2,3 etc..
    int index = s_index/(height*width) ;
    
    strcpy(pic_name, "op_");
    sprintf(pic_num,"%d", index);
    strcat(pic_name, pic_num);
    strcat(pic_name, "_");
    sprintf(pic_num, "%d", chunk_count);
    strcat(pic_name, pic_num);
    strcat(pic_name, ".pgm");
    
    im_chunk = (int *)malloc(sizeof(int)*(width)*(height));
    pad_chunks(s_index, height, width, image, chunk_count, im_chunk, pic_name);
    //write_image_template(pic_name, im_chunk, width, height);
}

// arguments entered should follow './main numChunks'
int main(int argc, char **argv){
    char *fName = "Lenna_org_256.pgm", *chunkName = "test_image.pgm";
    int *image;
    int im_width, im_height, num_chunks = 2, remainder;
    struct timeval start, end;

    // Check for input arguments to main program execution
    if (argc >1){
        num_chunks = atoi(argv[2]);
        fName = argv[1];
        printf("# of arguments entered: %d\n", argc);
        printf("# of chunks to make: %d\n", num_chunks);
    }
    // Read image using image.h function
    read_image_template(fName, &image, &im_width, &im_height);
    
    // Detect if image had odd # of rows
    remainder = im_height%num_chunks;
    
    // Practice making image
    //write_image_template(chunkName, image, im_width, im_height);
    
    // Call function to split image into p-chunks 
    int i, height_chunk, starting_index;
    height_chunk = (int)im_height/num_chunks;
    gettimeofday(&start, NULL);
    for(i=0; i<num_chunks;i++){
        starting_index = i*height_chunk*im_width;

        // Check for odd numbered rows and change size of chunk accordingly
        if(remainder>0 && i == num_chunks-1){
                height_chunk += remainder;
        }

        split_chunks(starting_index, height_chunk, im_width, image, num_chunks);
    }
    gettimeofday(&end, NULL);
        printf("Time to run: %ld microseconds\n", ((end.tv_sec * 1000000 + end.tv_usec) 
        - (start.tv_sec * 1000000 + start.tv_usec)));
  return 0;
}

