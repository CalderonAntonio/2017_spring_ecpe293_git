#include "image.h"
#include <time.h>
#include <sys/time.h>

void split_chunks(int s_index, int height, int width, int *image, int chunk_count){
   // *image=(int *)malloc(sizeof(int)*(*im_width)*(*im_height));
    int *im_chunk;
    int i;
    char pic_num[4];
    char pic_name[100];
    int index = s_index/(height*width) ;
    
    strcpy(pic_name, "op_");
    sprintf(pic_num,"%d", index);
    strcat(pic_name, pic_num);
    strcat(pic_name, "_");
    sprintf(pic_num, "%d", chunk_count);
    strcat(pic_name, pic_num);
    strcat(pic_name, ".pgm");
    
    im_chunk = (int *)malloc(sizeof(int)*(width)*(height));
    
    for (i=0; i<(width*height);i++){
            im_chunk[i] = image[i+s_index];
    }
    
    write_image_template(pic_name, im_chunk, width, height);
}

// arguments entered should follow './main numChunks'
int main(int argc, char **argv){
    char *fName = "Lenna_org_256.pgm", *chunkName = "test_image.pgm";
    int *image;
    int im_width, im_height, num_chunks = 2;

    // Check for input arguments to main program execution
    if (argc >1){
        num_chunks = atoi(argv[2]);
        fName = argv[1];
        printf("# of arguments entered: %d\n", argc);
        printf("# of chunks to make: %d\n", num_chunks);
    }
    // Read image using image.h function
    read_image_template(fName, &image, &im_width, &im_height);
    
    // Practice making image
    //write_image_template(chunkName, image, im_width, im_height);
    
    // Call function to split image into p-chunks 
    int i, height_chunk, starting_index;
    height_chunk = (int)im_height/num_chunks;
    for(i=0; i<num_chunks;i++){
        starting_index = i*height_chunk*im_width;
        // Function call split_chunks(starting_index, height_of_chunk, width, image_array_pointer, num_chunks)
        split_chunks(starting_index,height_chunk,im_width,image, num_chunks);
    }
  return 0;
}

