/***************************************
High Performance Computing 
Project 1: Warm-up in C with 'ghosts'
David Duy Ngo

Current Progress: 
	1. Familiar with "image.h".
	2. Wrote test program with the library.

TO DO:
	1. Split Image with image_split
	2. Simulates Send/recv the “ghost” rows to/from the adjacent chunks.
	3. Pads the chunks with “ghost” columns.
	4. Writes each of the chunks to a PGM image.
	5. Time Code
	6. Optimize

NOTES:
	Save the padded chunks to files. Use the following nomenclature:

	<op>_<chunk number>_<p>.pgm

	For example: op_2_16.pgm is 3rd chunk (chunks start from 0) when 16 chunks are created.
****************************************/
#include "image.h"
#include <time.h>
#include <sys/time.h>

/*
Notable Functions from image.h

void read_image_template(char *name, int **image, int *im_width, int *im_height);
void write_image_template(char *name, int *image, int im_width, int im_height);
*/

void image_split(){

}

//./main <file_name> <thread_count>
int main(int argc, char** argv){
	char *name = "Lenna_org_512.pgm";
	int **image_buffer;
	int im_width, im_height, thread_count;

	if (argc > 1){
		name = argv[1];
		thread_count = atoi(argv[2]);
	}
	
	read_image_template(name, image_buffer, &im_width, &im_height);

	write_image_template("test_image.pgm", *image_buffer, im_width, im_height); 
	
	return 0;
}
