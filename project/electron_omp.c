#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <time.h>
#include <sys/time.h>
	//#include <pthread.h>
#include "math.h"
#include <omp.h> // used for Open MP parallelization
# define n_electrons 1000 // number of electron particles
#define T 12
float randomFloat()
{
	float r = (float)rand() / (float)RAND_MAX;
	return r;
}

int main(int argc, char **argv)
{
	// From University of Illinois
	// http://uigelz.eecs.umich.edu/pub/short_courses/MCSHORT_0502.pdf 
	
	// Initialize random number generators. 
	// IMPORTANT: Use a different sequence of random numbers for different processes
	//for (int i =0; i<3; i++){
		//call init_ran(i)
	//}
	srand(time(NULL)); // may need to be seeded once per thread
	struct timeval start,end; // for timing
	float 	mass_e = 0.911E-27; // Electron mass (g)
	float 	mass_n = 20.*1.67E-24; // Atomic mass (g)
	
	// Select electric field and collision frequency
	float 	e_field = 5.0; // V/cm assumed in the x-direction
	float 	col_freq = 1.0E9; // collision frequency (1/s) 
	float 	q = 1.6E-12; // electron charge (C)
	float	accel_x = q*e_field/mass_e; // acceleration = F/m = qE/m
	float 	x; // position of electron (cm)
	float	*xn; // pointer to store electron positions
	float 	x_end = 20. ; // Drift distance (cm)
	float 	t_total; // Drift time (s)
	float 	v_drift = 0. ; // Drift velocity
	float 	v_x, v_y, v_z;
	float	vD_sum = 0., vD_avg;
	float 	*vD,*vx, *vy, *vz; // pointers to store electron velocities
	int 	norm = 0, num_threads; // Normalization constant 
	
	// malloc pointers
	vx	=(float *)malloc(sizeof(float)*n_electrons);
	vy	=(float *)malloc(sizeof(float)*n_electrons);
	vz	=(float *)malloc(sizeof(float)*n_electrons);
	vD	=(float *)malloc(sizeof(float)*n_electrons);
    xn	=(float *)malloc(sizeof(float)*n_electrons);
	
	/***Set OpenMp Environment Variables***/
	num_threads = omp_get_num_procs();
	if (num_threads > 4) 
	{
		omp_set_num_threads(num_threads/T);
		printf("\nOpenMPI found %d processors to create %d pthreads.\n",num_threads, num_threads/T);

	}
	else
	{
		omp_set_num_threads(num_threads);
		printf("\nOpenMPI found %d processors to create %d pthreads.\n",num_threads, num_threads);
	}
    
    //Computation starts here
    gettimeofday(&start,NULL);
	
	#pragma omp parallel for private(x, v_x, v_y, v_z, t_total)
	for (int i = 0; i< n_electrons; i++)
	{
		x = 0.; // Each electron starts at x =0 cm
		xn[i] = 0.;
		t_total = 0.;  
		v_x = 0.;
		v_y = 0.;
		v_z = 0.;
		vx[i] = 0.;
		vy[i] = 0.;
		vz[i] = 0.;
		float v;
		float alpha, beta, theta, phi;
		// update electron position and speed while x < x_end
		while (x < x_end){
			// Randomly choose time to next collision
			float dt= -(1./col_freq)*log(1. - randomFloat());
			
			// Update position, speed of particle
			x 		+= v_x*dt + 0.5*accel_x * pow(dt, 2);
			xn[i] 	+= vx[i]*dt + 0.5*accel_x * pow(dt, 2);
			v_x		+= accel_x*dt;
			vx[i]	+= accel_x * dt;
			t_total += dt; // moved to after after scattering (line 85)
		
			// Perform scatter if electrn has not reached x_end
			if (x < x_end)
			{
				v 		= sqrt(pow(v_x,2) + pow(v_y, 2) + pow(v_z,2));
				// Eularian Angles
				beta	= atan2(v_y, v_x);
				alpha 	= acos(v_z/v);

				// Randomly choose scattering angles
				theta 	= acos(2.*randomFloat() - 1.);
				//phi = 2*pi*rand(3);
				phi 	= 2*M_PI*randomFloat();

				// Account for energy loss during elastic collision 
				v 		= v*sqrt(1. - (2.*mass_e/mass_n)*(1.-cos(theta)));
				// Perform scatter
				v_x 	= v*(cos(beta)*cos(alpha)*sin(theta)*cos(phi) + cos(beta)*sin(alpha)*cos(theta) - sin(beta)*sin(theta)*sin(phi));
				vx[i]	=v*(cos(beta)*cos(alpha)*sin(theta)*cos(phi) + cos(beta)*sin(alpha)*cos(theta) - sin(beta)*sin(theta)*sin(phi));
				v_y 	= v*(sin(beta)*cos(alpha)*sin(theta)*cos(phi) + sin(beta)*sin(alpha)*cos(theta) - cos(beta)*sin(theta)*sin(phi)); 
				vy[i]	= v*(sin(beta)*cos(alpha)*sin(theta)*cos(phi) + sin(beta)*sin(alpha)*cos(theta) - cos(beta)*sin(theta)*sin(phi)); 
				v_z		= v*(-sin(alpha)*sin(theta)*cos(phi) + cos(alpha)*cos(theta)); 
				vz[i]	= v*(-sin(alpha)*sin(theta)*cos(phi) + cos(alpha)*cos(theta));

				//t_total += dt;
				/********** WORK ENDED HERE **************/
				// Advance trajectories until next collision
				//GO TO 43 //dt =  1./col_freq;
			}// END IF
			
		}// END WHILE (electron has drifted at least x_end)

		v_drift	+= x/t_total;
		vD[i]	=	xn[i]/t_total;
		vD_sum	+= vD[i];
		norm 	+= 1.;
		// END FOR
		/******/ //printf("speed of electron %d is %.3f / %.7f =%.0f\n",i, x,t_total, x/t_total); 
	}
	float v_temp= v_drift;
	v_drift 	= (float)v_temp/(float)norm; //take average velocity
	vD_avg		= (float)vD_sum/(float)norm;
	gettimeofday(&end,NULL);
	 
	printf("\n\033[31;1m	original V_DRIFT = %.1f cm/s		\033[0m \n", v_drift); 
	printf("\n\033[31;1m	pointers V_DRIFT = %.1f cm/s		\033[0m \n", vD_avg); 
    printf("\n\033[32;1mExecution time in ms: %ld 	\033[0m \n", ((end.tv_sec * 1000 + end.tv_usec/1000)
  		  - (start.tv_sec * 1000 + start.tv_usec/1000)));
	return 0;
}