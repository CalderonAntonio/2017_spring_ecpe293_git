#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <time.h>
#include <sys/time.h>
#include "math.h"
#include <cuda.h> // used for GPGPU parallelization
/* we need these includes for CUDA's random number stuff */
#include <curand.h>
#include <curand_kernel.h>

#define T 16
#define n_electrons 1000

__global__ void initCurand(curandState *state, unsigned long seed){
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(seed, idx, 0, &state[idx]);
}

__global__ void getrandFloat(curandState *state, float *a){
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
   	a[idx] = curand_uniform(&state[idx]);
}

__global__ void getrandFloat2(unsigned long seed, float *a){
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    curandState state;
    curand_init(seed, idx, 0, &state);
    a[idx] = curand_uniform(&state);
}

__global__
void scatter(float m_n, float m_e, float colF,float accel, float* rand_dt, float* rand_theta, float* rand_phi, float* vD)
{
	//for (int i = 0; i< n_electrons; i++)
	//{ 
	int 	i 		= threadIdx.x + blockIdx.x * blockDim.x;
	float 	x 		= 0.; // Each electron starts at x =0 cm
	float	x_end 	= 20. ; // Drift distance (cm)
	float 	t_total = 0.;  
	float 	v_x 	= 0.;
	float 	v_y 	= 0.;
	float 	v_z 	= 0.;
	float 	v	  	= 0.;
	//float v;
	float dt, alpha, beta, theta, phi;
	// update electron position and speed while x < x_end
	//printf("Entering while loop...\n");
	while (x < x_end){
		// Randomly choose time to next collision
		dt= -(1./colF)*logf(1. - rand_dt[i]);
		//printf("dt: %f\n", dt);
		// Update position, speed of particle
		x 		+= v_x*dt + 0.5*accel * powf(dt, 2.);
		v_x		+= accel*dt;
		t_total += dt;
	
		// Perform scatter if electrn has not reached x_end
		if (x < x_end)
		{
			v 		= sqrtf(powf(v_x,2.) + powf(v_y, 2.) + powf(v_z,2.));
			// Eularian Angles
			beta	= atan2f(v_y, v_x);
			alpha 	= acosf(v_z/v);

			// Randomly choose scattering angles
			theta 	= acosf(2.*rand_theta[i] - 1.);
			//phi = 2*pi*rand(3);
			phi 	= 2*M_PI*rand_phi[i];

			// Account for energy loss during elastic collision 
			v 		= v*sqrtf(1. - (2.*m_e/m_n)*(1.-cosf(theta)));
			// Perform scatter
			v_x 	= v*(cosf(beta)*cosf(alpha)*sinf(theta)*cosf(phi) + cosf(beta)*sinf(alpha)*cosf(theta) - sinf(beta)*sinf(theta)*sinf(phi));
			v_y 	= v*(sinf(beta)*cosf(alpha)*sinf(theta)*cosf(phi) + sinf(beta)*sinf(alpha)*cosf(theta) - cosf(beta)*sinf(theta)*sinf(phi)); 
			v_z		= v*(-sinf(alpha)*sinf(theta)*cosf(phi) + cosf(alpha)*cosf(theta)); 
			// Advance trajectories until next collision
		}// END IF
		
	}// END WHILE (electron has drifted at least x_end)
	vD[i]	=	x/t_total;
	__syncthreads();
}
// __global__ void avgOfPointer(){}
int main(int argc, char **argv)
{
	/**************************************************
	Scatter adapted from University of Illinois
	 http://uigelz.eecs.umich.edu/pub/short_courses/MCSHORT_0502.pdf 
	**************************************************/ 
	
	// Select electric field and collision frequency
	//int		n_electrons = 1000; // number of electron particles
	int 	num_threads = ceil((double) n_electrons/(T*T))*T*T;
	printf("Num Electrons: %d\n",n_electrons);
	printf("T: %d\n",T);
	printf("Num Threads: %f * %d ^2 = ",ceil((double) n_electrons/(T*T)),T);
	printf(" %d\n",num_threads);
	float 	e_field = 5.0; // V/cm assumed in the x-direction
	float 	col_freq = 1.0E9; // collision frequency (1/s) 
	float 	mass_e = 0.911E-27; // Electron mass (g)
	float 	mass_n = 20.*1.67E-24; // Atomic mass (g)
	float 	q = 1.6E-12; // electron charge (C)
	float	accel_x = q*e_field/mass_e; // acceleration = F/m = qE/m
	
	// Initialize random number generators. 
	srand(time(NULL)); // may need to be seeded once per thread
	struct timeval start,end; // for timing
	curandState *d_State;
	
	/*float 	x; // position of electron (cm)
	float	*xn, *d_xn; // pointer to store electron positions
	float 	t_total; // Drift time (s)
	float 	v_x, v_y, v_z;
	float	vD_sum = 0., vD_avg;
	*/
	float 	v_drift = 0. ; // Drift velocity
	float 	*vD, *d_vD; // pointers to store electron velocities
	float 	*rand_dt, *rand_theta, *rand_phi; 
	float 	*d_rand_dt, *d_rand_theta, *d_rand_phi; // pointers for rand
	float 	*d_rand_dt2, *d_rand_theta2, *d_rand_phi2;
	int 	norm = 0; // Normalization constant 
	
	// malloc pointers
	vD			=(float *)malloc(sizeof(float)*num_threads);
	rand_dt		=(float *)malloc(sizeof(float)*num_threads);
	rand_theta	=(float *)malloc(sizeof(float)*num_threads);
	rand_phi	=(float *)malloc(sizeof(float)*num_threads);
	cudaMalloc((void**)&d_State, sizeof(curandState)*num_threads);
	cudaMalloc((void **)&d_vD, sizeof(float)*num_threads);
	
	cudaMalloc((void **)&d_rand_dt, sizeof(float)*num_threads);
	cudaMalloc((void **)&d_rand_theta, sizeof(float)*num_threads);
	cudaMalloc((void **)&d_rand_phi, sizeof(float)*num_threads);
	
	cudaMalloc((void **)&d_rand_dt2, sizeof(float)*num_threads);
	cudaMalloc((void **)&d_rand_theta2, sizeof(float)*num_threads);
	cudaMalloc((void **)&d_rand_phi2, sizeof(float)*num_threads);
	
	// GPU Timing 
	float d_time;
	cudaEvent_t d_start, d_stop;

	cudaEventCreate(&d_start);
	cudaEventCreate(&d_stop);
	cudaEventRecord(d_start, 0);
	
    //Computation starts here
    gettimeofday(&start,NULL);
	
	initCurand<<<ceil(n_electrons/T*T),T*T>>>(d_State, time(NULL));
	getrandFloat<<<ceil(n_electrons/T*T),T*T>>>(d_State, d_rand_dt);
	getrandFloat<<<ceil(n_electrons/T*T),T*T>>>(d_State, d_rand_theta);
	getrandFloat<<<ceil(n_electrons/T*T),T*T>>>(d_State, d_rand_phi);
	
	getrandFloat2<<<ceil(n_electrons/T*T),T*T>>>(time(NULL), d_rand_dt2);
	getrandFloat2<<<ceil(n_electrons/T*T),T*T>>>(time(NULL), d_rand_theta2);
	getrandFloat2<<<ceil(n_electrons/T*T),T*T>>>(time(NULL), d_rand_phi2);
	/*
	cudaMemcpy(rand_dt,d_rand_dt, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	cudaMemcpy(rand_theta,d_rand_theta, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	cudaMemcpy(rand_phi,d_rand_phi, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	*/
	cudaMemcpy(rand_dt,d_rand_dt2, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	cudaMemcpy(rand_theta,d_rand_theta2, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	cudaMemcpy(rand_phi,d_rand_phi2, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	
	printf("Calling scatter kernel...\n");
	scatter<<<ceil(n_electrons/T*T),T*T>>>(mass_n,mass_e,col_freq,accel_x,d_rand_dt2,d_rand_theta2,d_rand_phi2,d_vD);
	printf("Finished scatter!\n");
	
	cudaMemcpy(vD,d_vD, sizeof(float)*num_threads, cudaMemcpyDeviceToHost);
	printf("Finished memCopy!\n");
	
	for (int i=0; i<num_threads; i++)
	{
		v_drift += vD[i];
		//printf("v: %f\n",vD[i]);
		norm ++;
	}
	printf("Finished summing!\n");
	
	
	printf("Random:\n");
	for (int i=0; i<num_threads; i++)
	{
		printf("dt: %f\n",rand_dt[i]);
		printf("theta: %f\n",rand_theta[i]);
		printf("phi: %f\n",rand_phi[i]);
	//	v_drift += vD[i];
	}
	
	cudaEventRecord(d_stop, 0);
	cudaEventSynchronize(d_stop);
	cudaEventElapsedTime(&d_time, d_start, d_stop);
    printf("\n\033[32;1mRand Num Generation time: %3.3f ms 	\033[0m \n", d_time);
	

		//v_drift	+= x/t_total;
		//vD[i]	=	xn[i]/t_total;
		//vD_sum	+= vD[i];
		//norm 	+= 1.;
		// END FOR
		/******/ //printf("speed of electron %d is %.3f / %.7f =%.0f\n",i, x,t_total, x/t_total); 
	float v_sum	= v_drift;
	v_drift 	= (float)v_sum/(float)norm; //take average velocity
	//vD_avg		= (float)vD_sum/(float)norm;
	gettimeofday(&end,NULL);
	 
	printf("\n\033[31;1m	original V_DRIFT = %.1f cm/s		\033[0m \n", v_drift); 
	//printf("\n\033[31;1m	pointers V_DRIFT = %.1f cm/s		\033[0m \n", vD_avg); 
    printf("\n\033[32;1mExecution time in ms: %ld 	\033[0m \n", ((end.tv_sec * 1000 + end.tv_usec/1000)
  		  - (start.tv_sec * 1000 + start.tv_usec/1000)));
	
	free(vD);
	cudaFree(d_State);
	cudaFree(d_vD);
	cudaFree(d_rand_dt);
	cudaFree(d_rand_theta);
	cudaFree(d_rand_phi);
	cudaFree(d_rand_dt2);
	cudaFree(d_rand_theta2);
	cudaFree(d_rand_phi2);
	return 0;
}