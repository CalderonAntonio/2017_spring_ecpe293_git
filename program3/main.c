/**********************************
 *  Author: Antonio Calderon
 *  ECPE 293 - High Performance Computing
 *  Program 3  - OpenMP: Magnitude and Phase
 * ********************************/
//#include "image.h"
#include "image_template.h"
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "math.h"
#include <unistd.h> 

struct thread_data{
    int chunk_width, chunk_height, image_height;
    int h_ker_w, h_ker_h, v_ker_w, v_ker_h, tid, offset;//ker_w/h change as apropriate
    float *chunk, *h_kernal, *v_kernal; // kernal changes from gaussian to derivative with apropriate convolve call
                        // or can use the value of t/pthread#/chunk (num_threads)to find location in global image
    float *gauss_horz, *grad_horz, *gauss_vert, *grad_vert;  /// output images will be grad_vert/horz                     
    //float *out_img;
    float *mag, *phase; // final output images to write
	void *status;
    char *message;
};

// Swaps values of two input floats
void swap(float *a, float *b)
{
	float t= *a;
	*a = *b;
	*b = t;
}

// outBound_check(): Returns 1 if the current position in kernal is good. Returns 0 if out of bounds;
// arguments (i,j,k,m, img_h, img_w, row_offset_i, col_offset_j) //ker_h, ker_w, ) ??
int outBound_check(int i, int j, int img_h, int img_w, int o_i, int o_j) 
{
	// check left boundry
	if((j+ o_j)<0)
		return 0;
	//check right
	else if((j+o_j)>=img_w)
		return 0;
	//check top
	else if((i+o_i)<0)
		return 0;
	//check bottom
	else if((i+o_i)>=img_h)
		return 0;
	else
		return 1; // should return 1 if all checks pass
}

void gaussian_kernal(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedcode to create kernals
    // [G,w]=GaussianKernel(sigma) // return values
    a= round(2.5*sigma - 0.5); //a=1
    *w=2*a+1; sum=0;  // w=3 
    *G = (float *)malloc(sizeof(float)*(*w));
    for (i=0; i<*w; i++){
        (*G)[i]=exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum+(*G)[i];      
    }
    
    //printf("Gausian_kernal: ");
    for (i=0; i<*w; i++){
        (*G)[i] = (*G)[i]/sum; //Normalize to get PDF area of 1
        printf("%f	",(*G)[i]);// Print Gaussian mask 
    }
    printf("\n");
}

void gaussian_derivative(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedocode to create derivative kernals
    // [Gderiv,w]=GaussianDerivativeKernel(sigma) // returns values
    a=round(2.5*sigma - 0.5);
    *w=2*a+1; sum=0;
    *G = (float *)malloc(sizeof(float)*(*w)); // this means passed in value is not yet malloc'ed
    for(i=0; i<*w; i++)
    {   
        (*G)[i]=-2*(i-a)*exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum-i*(*G)[i];
    }  
    
    //printf("Gausian_derivative_kernal: ");
    for (i=0; i<*w; i++) 
    {
        (*G)[i] = (*G)[i]/sum; //     Gderiv=Gderiv/sum 
        printf("%f	",(*G)[i]);  // print Gaussian derivative
    }
    printf("\n");
}

void convolve(float *img, float *ker, int width, int height, int img_height, int k_w, int k_h, int offset, float *out_img){// pass in pointer of Image/chunk, pointer of G_gauss and/or G_deriv, image_width/height, kernal_width/height, index offset of chunk, double pointer to output image 

	float sum;
    int offset_i, offset_j;
    int i,j,k,m;
    int i_img = offset/width; // offset of i in chunk to full image
	//*out_img = (float *)malloc(sizeof(float)*width*height);
    //Begin looping thru image (chunk)
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            sum =0. ; // reset SOP at each image pixel
            // begin looping thru kernal element
            for(k=0;k<k_h;k++)
            {
                for(m=0;m<k_w;m++)
                {
                    offset_i = -1*floor(k_h/2)+k;
                    offset_j = -1*floor(k_w/2)+m;
                    int tempCheck = outBound_check(i+i_img,j,img_height,width,offset_i,offset_j);
                    if( tempCheck==1){
                        //printf("i:%d j:%d\n",i+offset_i, j+ offset_j);
                        sum += img[((i+offset_i)*width)+(j+offset_j)]*ker[k*k_w+m];
                    }
                    else
                        sum += 0;
                }
            }
            (out_img)[i*width+j] = sum;
        }
    }
}

void *parallel_gauss(void *argument)
{
    char picName[100], picNum[4];//"./test_img/horizontal_gradient.pgm", *vertfName = "./test_img/vertical_gradient.pgm";
    struct thread_data *tdata;
    tdata =(struct thread_data *)argument;
    //float *temp = (float *)malloc(sizeof(float)*tdata->chunk_width*tdata->image_height);
    //printf("Thread %d says %s\n", tdata->tid, tdata->message);
    
    // define name of intermediate chunk image files
    strcpy(picName, "../test_img/");
    strcat(picName, tdata->message);
    sprintf(picNum,"_%d", tdata->tid);
    strcat(picName, picNum);
    strcat(picName, ".pgm");

    // convolve horizontal image
    convolve(tdata->chunk, tdata->h_kernal, tdata->chunk_width, tdata->chunk_height, tdata->image_height, tdata->h_ker_w, tdata->h_ker_h, tdata->offset, tdata->gauss_horz);//tdata->gauss_horz); // for parallelization, use generalized pointers which can be reused??? (eg &tdata->temp)
	//write_image_template<float>(picName, tdata->temp, tdata->chunk_width, tdata->chunk_height);

	// convolve vertical image
	convolve(tdata->chunk, tdata->v_kernal, tdata->chunk_width, tdata->chunk_height, tdata->image_height, tdata->v_ker_w, tdata->v_ker_h, tdata->offset, tdata->gauss_vert); 
    
	// Write intermediate chunk files for troubleshooting
	//write_image_template<float>(picName, tdata->gauss_vert, tdata->chunk_width, tdata->image_height);
    //   free(temp);
    pthread_exit(NULL);
}

void *parallel_grad(void *argument)
{
    char picName[100], picNum[4];//"./test_img/horizontal_gradient.pgm", *vertfName = "./test_img/vertical_gradient.pgm";
    struct thread_data *tdata;
    tdata =(struct thread_data *)argument;
    //float *temp = (float *)malloc(sizeof(float)*tdata->chunk_width*tdata->image_height);
    //printf("Thread %d says %s\n", tdata->tid, tdata->message);
    
    // define name of intermediate chunk image files
    strcpy(picName, "../test_img/");
    strcat(picName, tdata->message);
    sprintf(picNum,"_%d", tdata->tid);
    strcat(picName, picNum);
    strcat(picName, ".pgm");

    // convolve horizontal image
    convolve(tdata->gauss_horz, tdata->h_kernal, tdata->chunk_width, tdata->chunk_height, tdata->image_height, tdata->h_ker_w, tdata->h_ker_h, tdata->offset, tdata->grad_horz);
	//write_image_template<float>(picName, tdata->temp, tdata->chunk_width, tdata->chunk_height);

	// convolve vertical image
	convolve(tdata->gauss_vert, tdata->v_kernal, tdata->chunk_width, tdata->chunk_height, tdata->image_height, tdata->v_ker_w, tdata->v_ker_h, tdata->offset, tdata->grad_vert); 
    
	// Write intermediate chunk files for troubleshooting
	//write_image_template<float>(picName, tdata->gauss_vert, tdata->chunk_width, tdata->image_height);
    //   free(temp);
    pthread_exit(NULL);
}

void *parallel_mag(void *argument) //argument should contain img_height, img_width, grad_horz, grad_vert, *magnitude
{
    struct thread_data *tdata;
    tdata      = (struct thread_data *)argument;
	int width  = tdata->chunk_width;
	int height = tdata->image_height;
	float *out = tdata->mag;
	float *h_g = tdata->grad_horz;
	float *v_g = tdata->grad_vert;
	for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
			out[i*width + j]=sqrt( pow(v_g[i*width +j],2) + pow(h_g[i*width +j],2));
		}
	}
	pthread_exit(NULL);
}

void *parallel_phase(void *argument)//argument should contain img_height, img_width, grad_horz, grad_vert, *phase
{
    struct thread_data *tdata;
    tdata =(struct thread_data *)argument;
	int width = tdata->chunk_width;
	int height = tdata->image_height;
	float *out = tdata->phase;
	float *h_g = tdata->grad_horz;
	float *v_g = tdata->grad_vert;
	for(int i=0; i< height; i++)
	{
		for(int j=0; j< width; j++) 
		{
	        out[i*width + j]=atan2(v_g[i*width + j], h_g[i*width + j]);
		}
	}
	pthread_exit(NULL);
}
int main(int argc, char **argv){
    char *fName = "../LENNA_IMAGES/Lenna_org_256.pgm"; //default image if no comand line argument
    char magfName[100];// = "./test_img/horizontal_gradient.pgm"
    char phasefName[100]; //= "./test_img/vertical_gradient.pgm"; // filename of output gradients
    char picNum[4];
	float *temp_vert, *temp_horz,  *image;//*h_Gradient, *v_Gradient,
    float  sigma =0.5;
    int im_width, im_height,w,v_ker_width, v_ker_height, h_ker_width, h_ker_height, offset=0, num_threads = 4;
    int t,rc, remainder; 
    float *v_kernal, *h_kernal;
    float *v_deriv, *h_deriv;
    float *gauss_horz, *gauss_vert;
    float *grad_horz, *grad_vert;
	float *magnitude, *phase;
    //float *out_img;
    struct timeval start, end; // for timing  
	
    if (argc !=4)
    {
        printf("Correct argument list is: ./exec <imagePath> <sigma value> <num of pthreads>\n");
	 	return 0;
    }
	if (argc >1){
            fName = argv[1];
            sigma = atof(argv[2]);
            num_threads = atoi(argv[3]);
        	//printf("# of arguments entered: %d\n", argc);
        	printf("# of pthreads to create: %d\n", num_threads);
    }
    // Start timing
    gettimeofday(&start, NULL);
    // Read image using image_template.h function
    read_image_template(fName, &image, &im_width, &im_height);
    // Detect if image had odd # of rows
    remainder = im_height%num_threads;
 
	// VERTICAL GAUSSIAN
	printf("\n** VERTICAL GAUSSIAN **\n");
    // Create horizontal gaussian kernal from sigma
    gaussian_kernal(sigma, &v_kernal, &w); 
	v_ker_width = w; v_ker_height = 1; // define kernal dimensions for specific gradient
	printf("vert_kernal width:%d height:%d\n",v_ker_width,v_ker_height);
    
    // HORZONTAL GAUSSIAN
	printf("\n** HORIZONTAL GAUSSIAN **\n");
    // Create vertical gaussian kernal from sigma
    gaussian_kernal(sigma, &h_kernal, &w); 
	h_ker_width = 1; h_ker_height = w; // define kernal dimensions for specific gradient
	printf("horz_kernal width:%d height:%d\n",h_ker_width,h_ker_height);
    
    // Call pthreads for GAUSSIAN image
    struct thread_data tdata[num_threads];
    pthread_t threads[num_threads];
    pthread_attr_t attr;
    //out_img = (float *)malloc(sizeof(float)*im_width*im_height);
    gauss_horz = (float *)malloc(sizeof(float)*im_width*im_height);
    gauss_vert = (float *)malloc(sizeof(float)*im_width*(im_height));
    
    //Initialize and set thread detached attribute 
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	// Call pthread for parallel_gauss() with appropriate tdata[t] values initialized
    for(t=0;t<num_threads;t++)
    {
        tdata[t].tid = t;
        tdata[t].chunk_width = im_width;
        tdata[t].chunk_height = im_height/num_threads;	//Begin looping thru image (chunk)
        tdata[t].image_height = im_height;
		tdata[t].offset = t*(tdata[t].chunk_width)*(tdata[t].chunk_height);
		tdata[t].chunk =  image + tdata[t].offset;
		
        tdata[t].v_kernal = v_kernal;
        tdata[t].h_kernal = h_kernal;
        tdata[t].h_ker_w = h_ker_width;
        tdata[t].h_ker_h = h_ker_height;
        tdata[t].v_ker_w = v_ker_width;
        tdata[t].v_ker_h = v_ker_height;

        tdata[t].message = "Sup_vert";
        //tdata[t].out_img = out_img +tdata[t].offset;
        tdata[t].gauss_vert = gauss_vert + tdata[t].offset; 
        tdata[t].gauss_horz = gauss_horz + tdata[t].offset; 
        //rc = pthread_create(&threads[t], NULL, PrintHello, (void *)&tdata[t]);
        rc = pthread_create(&threads[t], &attr, parallel_gauss, (void *)&tdata[t]);
        if (rc) {
          printf("ERROR; return code from pthread_creat() is %d\n", rc);
          exit(-1);
        }
       // printf("pthread error: %d\n",rc);
    }
	pthread_attr_destroy(&attr);
	for(t=0;t<num_threads;t++)
    {
        rc = pthread_join(threads[t], &tdata[t].status);
        if (rc) {
          printf("ERROR; return code from pthread_join() is %d\n", rc);
          exit(-1);
        }
        //printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)tdata[t].status);
    }
    //Define output filenames based on test parameters
    /*
    strcpy(horzfName, "./test_img/horzGAUSS_");
    sprintf(picNum,"%d__s%.2f_p%d",im_width,sigma,num_threads); strcat(horzfName, picNum);
    strcat(horzfName, ".pgm");
    strcpy(vertfName, "./test_img/vertGAUSS_");
    strcat(vertfName, picNum);
    strcat(vertfName, ".pgm");
    write_image_template<float>(vertfName, gauss_vert, im_width, im_height);
    write_image_template<float>(horzfName, gauss_horz, im_width, im_height);
    */
	
	/******BEGIN SECTION: for calling second parallel convolve function *******/
	// VERTICAL DERIVATIVE
	printf("\n** VERTICAL DERIVATIVE **\n");
    gaussian_derivative(sigma, &v_deriv, &w);
	v_ker_width = 1; v_ker_height = w; 
	printf("vert_derivative width:%d height:%d\n",v_ker_width,v_ker_height);
    for (int i=0; i<w/2; i++) // Flip derivative kernal
		swap(&v_deriv[i], &v_deriv[w-i-1]);

	// HORIZONTAL DERIVATIVE
	printf("\n** HORIZONTAL DERIVATIVE **\n");
    gaussian_derivative(sigma, &h_deriv, &w);
	h_ker_width = w; h_ker_height = 1; 
	printf("horz_derivative width:%d height:%d\n",h_ker_width,h_ker_height);
    for (int i=0; i<w/2; i++) // Flip derivative kernal
		swap(&h_deriv[i], &h_deriv[w-i-1]);

    // Call pthreads for deriv convolve for GRADIENT image
        /*
        struct thread_data tdata[num_threads];
        pthread_t threads[num_threads];
        */
    grad_horz = (float *)malloc(sizeof(float)*im_width*im_height);
    grad_vert = (float *)malloc(sizeof(float)*im_width*im_height);
	
    //RE-Initialize and set thread detached attribute 
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for(t=0;t<num_threads;t++)
    {
        /*
        tdata[t].tid = t;
        tdata[t].chunk_width = im_width;
        tdata[t].chunk_height = im_height/num_threads;	//Begin looping thru image (chunk)
        tdata[t].image_height = im_height;
		tdata[t].offset = t*(tdata[t].chunk_width)*(tdata[t].chunk_height);
		tdata[t].chunk =  image + tdata[t].offset;
        */
		tdata[t].v_kernal = v_deriv;
		tdata[t].h_kernal = h_deriv;
        tdata[t].h_ker_w = h_ker_width;
        tdata[t].h_ker_h = h_ker_height;
        tdata[t].v_ker_w = v_ker_width;
        tdata[t].v_ker_h = v_ker_height;

        tdata[t].message = "Sup_horz";
        tdata[t].grad_vert = grad_vert + tdata[t].offset; 
        tdata[t].grad_horz = grad_horz + tdata[t].offset; 
        //rc = pthread_create(&threads[t], NULL, parallel_gauss, (void *)&tdata[t]);
        rc = pthread_create(&threads[t], &attr, parallel_grad, (void *)&tdata[t]);
        if (rc) {
          printf("ERROR; return code from pthread_creat() is %d\n", rc);
          exit(-1);
        }
        //printf("pthread error: %d\n",rc);
    }
	pthread_attr_destroy(&attr);
    for(t=0;t<num_threads;t++)
    {
        rc = pthread_join(threads[t], &tdata[t].status);
        if (rc) {
          printf("ERROR; return code from pthread_join() is %d\n", rc);
          exit(-1);
        }
        //printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)tdata[t].status);      
    }
	
	/******BEGIN SECTION: for calling parallel magnitude function *******/
	// Initialize MAGNITUDE pointer
    magnitude = (float *)malloc(sizeof(float)*im_width*im_height);
	// Reinitialize attr for MAGNITUDE and PHASE threads
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	// Call pthread for parallel_gauss() with appropriate tdata[t] values initialized
    for(t=0;t<num_threads;t++)
    {
        tdata[t].message = "Sup_mag";
        tdata[t].grad_vert = grad_vert; 
        tdata[t].grad_horz = grad_horz; 
		tdata[t].mag = magnitude;
        rc = pthread_create(&threads[t], &attr, parallel_mag, (void *)&tdata[t]);
        if (rc) {
          printf("ERROR; return code from pthread_creat() is %d\n", rc);
          exit(-1);
        }
       // printf("pthread error: %d\n",rc);
    }
	pthread_attr_destroy(&attr);
	for(t=0;t<num_threads;t++)
    {
        rc = pthread_join(threads[t], &tdata[t].status);
        if (rc) {
          printf("ERROR; return code from pthread_join() is %d\n", rc);
          exit(-1);
        }
        //printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)tdata[t].status);
    }
    //Define output filenames based on test parameters
    strcpy(magfName, "./test_img/magnitude_");
    sprintf(picNum,"%d__s%.2f_p%d",im_width,sigma,num_threads); 
	strcat(magfName, picNum);
    strcat(magfName, ".pgm");
    write_image_template<float>(magfName, magnitude, im_width, im_height);
	
	
	/***BEGIN SECTION: for calling parallel phase function ***/
	
	// Initialize MAGNITUDE and PHASE pointers
    phase = (float *)malloc(sizeof(float)*im_width*im_height);
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	// Call pthread for parallel_gauss() with appropriate tdata[t] values initialized
    for(t=0;t<num_threads;t++)
    {
        tdata[t].message = "Sup_phase";
		tdata[t].phase = phase;
        rc = pthread_create(&threads[t], &attr, parallel_phase, (void *)&tdata[t]);
        if (rc) {
          printf("ERROR; return code from pthread_creat() is %d\n", rc);
          exit(-1);
        }
       // printf("pthread error: %d\n",rc);
    }
	pthread_attr_destroy(&attr);
	for(t=0;t<num_threads;t++)
    {
        rc = pthread_join(threads[t], &tdata[t].status);
        if (rc) {
          printf("ERROR; return code from pthread_join() is %d\n", rc);
          exit(-1);
        }
        //printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)tdata[t].status);
    }
	
	strcpy(phasefName, "./test_img/phase_");
    strcat(phasefName, picNum);
    strcat(phasefName, ".pgm");
    write_image_template<float>(phasefName, phase, im_width, im_height);
    
 
    free(h_kernal);
    free(v_kernal);

    //free(out_img);
    free(gauss_horz);
    free(gauss_vert);
    free(grad_horz);
    free(grad_vert);
	free(magnitude);
	free(phase);
    // End Timing
     gettimeofday(&end, NULL);
        printf("\n\033[32;1m Time to run: %ld milliseconds\033[0m\n\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000);
    
    return 0;
}
