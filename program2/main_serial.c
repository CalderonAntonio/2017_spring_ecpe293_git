#include "image_template.h"
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include "math.h"
#include <unistd.h> 

// Swaps values of two input floats
void swap(float *a, float *b)
{
	float t= *a;
	*a = *b;
	*b = t;
}

// outBound_check(): Returns 1 if the current position in kernal is good. Returns 0 if out of bounds;
// Use in main() by mupltiplying function with SOP iteration
// arguments (i,j,k,m, img_h, img_w, row_offset_i, col_offset_j) //ker_h, ker_w, ) ??
int outBound_check(int i, int j, int img_h, int img_w, int o_i, int o_j) 
{
	// check left boundry
	if((j+ o_j)<0)
		return 0;
	//check right
	else if((j+o_j)>=img_w)
		return 0;
	//check top
	else if((i+o_i)<0)
		return 0;
	//check bottom
	else if((i+o_i)>=img_h)
		return 0;
	else
		return 1; // should return 1 if all checks pass
}

void gaussian_kernal(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedcode to create kernals
    // [G,w]=GaussianKernel(sigma) // return values
    a= round(2.5*sigma - 0.5); //a=1
    *w=2*a+1; sum=0;  // w=3 
    *G = (float *)malloc(sizeof(float)*(*w));
    for (i=0; i<*w; i++){
        (*G)[i]=exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum+(*G)[i];      
    }
    
    //printf("Gausian_kernal: ");
    for (i=0; i<*w; i++){
        (*G)[i] = (*G)[i]/sum; //Normalize to get PDF area of 1
        printf("%f	",(*G)[i]);// Print Gaussian mask 
    }
    //printf("\n");
}

void gaussian_derivative(float sigma, float **G, int *w){
    float sum;
    int a, i;
    // Psuedocode to create derivative kernals
    // [Gderiv,w]=GaussianDerivativeKernel(sigma) // returns values
    a=round(2.5*sigma - 0.5);
    *w=2*a+1; sum=0;
    *G = (float *)malloc(sizeof(float)*(*w)); // this means passed in value is not yet malloc'ed
    for(i=0; i<*w; i++){   
        (*G)[i]=-2*(i-a)*exp((-1*(i-a)*(i-a))/(2*sigma*sigma));     
        sum=sum-i*(*G)[i];
    }  
    
    //printf("Gausian_derivative_kernal: ");
    for (i=0; i<*w; i++) {
        (*G)[i] = (*G)[i]/sum; //     Gderiv=Gderiv/sum 
        //printf("%f	",(*G)[i]);  // print Gaussian derivative
    }
    //printf("\n");
}

void convolve(float *img, float *ker, int width, int height, int k_w, int k_h, float **out_img){// pass in pointer of Image, pointer of G_gauss and/or G_deriv, image_width/length, kernal_width/height, double pointer to output image 

	float sum;
    int offset_i, offset_j;
    int i,j,k,m;
	
	*out_img = (float *)malloc(sizeof(float)*width*height);
    //Begin looping thru image (chunk)
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            sum =0. ; // reset SOP at each image pixel
            // begin looping thru kernal element
            for(k=0;k<k_h;k++)
            { 
                for(m=0;m<k_w;m++)
                {
                    offset_i = -1*floor(k_h/2)+k;
                    offset_j = -1*floor(k_w/2)+m;
                    int tempCheck = outBound_check(i,j,height,width,offset_i,offset_j);
                    if( tempCheck==1)
                    {
                        sum += img[((i+offset_i)*width)+(j+offset_j)]*ker[k*k_w+m];
                        //if(k>2) printf("k:%d m:%d\n Value of Image:%f Kernal:%f \n", k, m,img[i*width+j],ker[k*k_w+m]);
                    }
                    else
                        sum += 0;
                    
                }
            }
            (*out_img)[i*width+j] = sum;
        }
    }
}

int main(int argc, char **argv){
    char *fName = "../LENNA_IMAGES/Lenna_org_256.pgm"; //default image if none comand line argument
    char horzfName[100];// = "horizontal_gradient.pgm", 
    char vertfName[100];// = "vertical_gradient.pgm"; // output filenames
    char picNum[4];
	float *temp_vert, *temp_horz, *h_Gradient, *v_Gradient, *image;
    float *G_kernal, *G_deriv, sigma =0.5;
    int im_width, im_height,w,w_ker, h_ker,w_der, h_der, num_threads = 4;
    int t,rc, remainder;
	struct timeval start, end; // for timing
    
    if (argc !=3)
    {
        printf("Correct argument list for main_serial.c is: ./exec <imagePath> <sigma value>\n");
	 	return 0;
    }
	if (argc >1){
            fName = argv[1];
            sigma = atof(argv[2]);
    }
    //Start timing
    gettimeofday(&start, NULL);
    // Read image using image_template.h function
    read_image_template(fName, &image, &im_width, &im_height);
    
    //Define output filenames based on test parameters
    strcpy(horzfName, "./test_img/S_horzGrad_");
    sprintf(picNum,"%d_s%.2f",im_width,sigma); strcat(horzfName, picNum);
    strcat(horzfName, ".pgm");
    
    strcpy(vertfName, "./test_img/S_vertGrad_");
    strcat(vertfName, picNum);
    strcat(vertfName, ".pgm");
	// VERTICAL GRADIENT
	printf("\n**** VERTICAL GRADIENT ****\n");
    // Create horizontal gaussian kernal from sigma
    gaussian_kernal(sigma, &G_kernal, &w); 
	w_ker = w; h_ker = 1; // define kernal dimensions for specific gradient
	printf("kernal width:%d height:%d\n",w_ker,h_ker);
    
	// create vertical derivative kernal
    gaussian_derivative(sigma, &G_deriv, &w);
	w_der = 1; h_der = w; 
	printf("derivative width:%d height:%d\n",w_der,h_der);
    for (int i=0; i<w/2; i++) // Flip derivative kernal
		swap(&G_deriv[i], &G_deriv[w-i-1]);
    /*printf("Gausian_derivative_kernal flipped: ");
    for (int i=0; i<w; i++)
        printf("%f	",G_deriv[i]);  // print flipped Gaussian derivative
    printf("\n");*/
	
	char *tempName = "step1_vert.pgm";
    convolve(image, G_kernal, im_width, im_height, w_ker, h_ker, &temp_vert); // for parallelization, use generalized pointers which can be reused??? (eg &temp_gradient)
	//write_image_template<float>(tempName, temp_vert, im_width, im_height);
	convolve(temp_vert, G_deriv, im_width, im_height, w_der, h_der, &v_Gradient);
    write_image_template<float>(vertfName, v_Gradient, im_width, im_height);
	free(G_kernal); // free mask memory as they will be malloc'ed again when kernal; functions are called again
	free(G_deriv);
	
	// HORZONTAL GRADIENT
	printf("\n**** HORIZONTAL GRADIENT ****\n");
    // Create vertical gaussian kernal from sigma
    gaussian_kernal(sigma, &G_kernal, &w); 
	w_ker = 1; h_ker = w; // define kernal dimensions for specific gradient
	printf("kernal width:%d height:%d\n",w_ker,h_ker);
    
	// create horizontal derivative kernal
    gaussian_derivative(sigma, &G_deriv, &w);
	w_der = w; h_der = 1; 
	printf("derivative width:%d height:%d\n",w_der,h_der);
    for (int i=0; i<w/2; i++) // Flip derivative kernal
		swap(&G_deriv[i], &G_deriv[w-i-1]);
    /*printf("Gausian_derivative_kernal flipped: ");
    for (int i=0; i<w; i++)
        printf("%f	",G_deriv[i]);  // print flipped Gaussian derivative
    printf("\n");*/
	
	tempName = "step1_horz.pgm";
    convolve(image, G_kernal, im_width, im_height, w_ker, h_ker, &temp_horz);
	//write_image_template<float>(tempName, temp_horz, im_width, im_height);
	convolve(temp_horz, G_deriv, im_width, im_height, w_der, h_der, &h_Gradient);
    write_image_template<float>(horzfName, h_Gradient, im_width, im_height);
    // End Timing
     gettimeofday(&end, NULL);
        printf("\n\033[32;1m Time to run: %ld milliseconds\033[0m\n\n", ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000);
    return 0;
}
