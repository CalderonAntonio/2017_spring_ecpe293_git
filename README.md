Welcome, this is my very first repository!
---------------------------------------------
This repository is both practice to learn bitbucket and Mercurial/Git
as well as submitting assignments for ECPE 293 - High erformance Computing

Completed assignments are listed below:

1) Ghosting in C: A Warm Up
Instructions: name of executable 'test' 
run by: ./test <name of imagefile> <number of chunks to create>

2) Image Intensity Gradients: Convolution Using Pthreads
****NOTE: code requires subdirectory ./test_img/ in same folder as main.c****
**** The 'test_img' folder should be included in 'git pull' repository but must be checked to prevent Seg_Fault 

compile main.c by: make
compile main_serial.c by command: make -f serial_Makefile

Run parallel:	./parallel <path to image> <sigma> <number of threads>
Run serial:		./serial <path to image> <sigma> 
3)
4)
5)

**Final Project**: Electron Transport - Monte Carlo Modeling
    all necessary files are under /project folder

This repo is accessible through SSH or HTTPS.